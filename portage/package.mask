# use libressl
#dev-libs/libevent::gentoo
dev-lang/python::gentoo
dev-lang/python::pg_overlay
dev-lang/ruby::gentoo
dev-lang/rust::gentoo
dev-lang/rust::pg_overlay
dev-lang/rust-bin
dev-perl/Net-SSLeay::gentoo
dev-libs/openssl::gentoo
#dev-libs/libevent
#dev-lang/python
#dev-lang/ruby
#dev-lang/rust
#dev-lang/rust-bin
#dev-perl/Net-SSLeay
#dev-libs/openssl
# go back to a static /dev
sys-fs/eudev
sys-fs/udev
virtual/udev
sys-auth/polkit
sys-auth/consolekit
sys-auth/rtkit
media-sound/pulseaudio
media-sound/pulseaudio-daemon
media-libs/libpulse
sys-apps/systemd
# purge systemd
#sys-apps/openrc::gentoo
sys-apps/systemd-utils
# gtk3 without dbus

#app-accessibility/at-spi2-core
>=dev-libs/atk-2.46
#>x11-libs/gtk+-3.14.13
#dev-libs/atk::gentoo
#dev-libs/atk::stefan_overlay
#x11-libs/gtk+::mv
x11-libs/gtk+::stefan_overlay
# other bloat
sys-libs/pam
sys-auth/pambase

## firefox debug mask

#www-client/firefox::pg_overlay
#www-client/librewolf::librewolf
#dev-libs/atk
